import React, {useState} from 'react';
import {
  Text,
  TouchableOpacity,
  Alert
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';

type Navigate = {
  navigation: any;
};

const QrScreen = (props: Navigate) => {
  const [scan, setScan] = useState(true);

  const alertBtn: {text: string; onPress: () => void}[] = [
    {
      text: 'Continue to Pin ',
      onPress: () =>{
        setScan(!scan),
        props.navigation.navigate('Pin')
      } ,
    },
    {
      text: 'Scan Again',
      onPress: () => {
        setScan(!scan);
      },
    },
  ];

  const ifScanned = (e: any) => {
    const check_for_htpp: string = e.data.substring(0, 4);

    if (check_for_htpp === 'http') {
      Alert.alert(
        'Qr code is Valid',
        `Data : ${e.data}\nType: ${e.type}\nTarget: ${e.target}`,
        alertBtn,
      );
    } else {
      Alert.alert(
        'Qr code is Invalid',
        `Data : ${e.data}\nType: ${e.type}\nTarget: ${e.target}`,
        [
          {
            text: 'Scan Again',
            onPress: () => {
              setScan(!scan);
            },
          },
        ],
      );
    }
  };

  return (
    <QRCodeScanner
      key={String(scan)}
      onRead={ifScanned}
      reactivate={false}
      permissionDialogMessage="This app is is asking for permission to use the Camera."
      showMarker={true}
      markerStyle={{borderColor: '#FFF', borderRadius: 10}}
      bottomContent={
        <TouchableOpacity>
          <Text style={{fontSize: 21, color: 'rgb(30,144,255)'}}>
            Scan QR Code
          </Text>
        </TouchableOpacity>
      }
    />
  );
};
export default QrScreen;
