import React, { useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Button,Alert,Text,SafeAreaView, StyleSheet, TextInput, View} from 'react-native';
import PinDisplayStyle from './PinDisplayStyle';



const  PinDisplay = ()=> {
    
    const[pinValue,setPinValue] = useState<string | null>(null)

    AsyncStorage.getItem("logedPin").then((value:string | null) => {
        if(typeof value === 'string'){
            setPinValue(value);
        }else{
            setPinValue("There appears to be an error try again"); 
        }
        
     })

  return (
    <View style={PinDisplayStyle.textView}>
      <Text
       style={PinDisplayStyle.text}
      >The Pin you entered: {"\n"}{pinValue}</Text>
    </View>   
  )
}
export default PinDisplay