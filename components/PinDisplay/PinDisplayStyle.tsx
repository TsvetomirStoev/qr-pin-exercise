import {StyleSheet} from "react-native";

const PinDisplayStyle = StyleSheet.create({
    textView:{
        paddingTop:300,
    },
    text: {
        color: '#00FF7F',
        fontSize:50,
        textAlign:'center'
    }
  });
  
  export default PinDisplayStyle;