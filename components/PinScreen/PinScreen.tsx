import React, { useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Button, TextInput, View} from 'react-native';
import PinScreenStyle from './PinScreenStyle';

type Navigate = {
  navigation:any
};

const  PinScreen = (props: Navigate)=> {
  const [text, onChangeText] = useState("");
  const disable = text.length !== 4 ;

  const onSubmit = ()=>{
    console.log(text)
    AsyncStorage.setItem("logedPin", text);
    props.navigation.navigate('Pin Display');
  };

  return (
    <View>
      <TextInput
          style={PinScreenStyle.input}
          secureTextEntry={true}
          onChangeText={onChangeText}
          value={text}
          keyboardType='numeric'
          maxLength={4}
        /> 
        <View style={PinScreenStyle.btnStyle}>
        <Button
          onPress={onSubmit}
          title="Enter Pin"
          disabled={disable}
        />  
        </View>  
    </View>   
  )
}
export default PinScreen

