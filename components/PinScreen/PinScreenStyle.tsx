
import {StyleSheet} from "react-native";


const PinScreenStyle = StyleSheet.create({
    input: {
        letterSpacing: 45,
        color: '#00FF7F',
        justifyContent: 'center',
        width: '80%',
        alignSelf: 'center',
        margin: 20,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        fontSize: 50,
        paddingTop:200,
        
        
    },
    btnStyle:{
        justifyContent: 'center',
        width: '80%',
        alignSelf: 'center',
        paddingTop:40
    }
  });
  
  export default PinScreenStyle;