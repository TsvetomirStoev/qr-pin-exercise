import React from 'react';
import {Button,Alert } from 'react-native';
import { NavigationContainer} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import QrScreen from './components/QrScreen/QrScreen';
import PinScreen from './components/PinScreen/PinScreen';
import PinDisplay from './components/PinDisplay/PinDisplay';

type Navigate = {
  navigation: any,
};

// navigationOptions:  {
//   title: 'Title',
//   headerLeft: null,
//   gesturesEnabled: false,
// }

const  App = (props: Navigate)=> {

  const Stack = createNativeStackNavigator();
  
  return (
    
     <NavigationContainer>
      <Stack.Navigator initialRouteName="QR Scanner">
        <Stack.Screen name="QR Scanner" component={QrScreen} />
        <Stack.Screen 
          name="Pin" 
          component={PinScreen}
         
        />
        <Stack.Screen name="Pin Display" component={PinDisplay} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App

